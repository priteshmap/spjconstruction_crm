import { Router } from "express";
import * as multer from 'multer';
import * as path from 'path';

import { AdminController } from "../controllers/admin/AdminController";
import { EmployeeController } from "../controllers/admin/EmployeeController";
import { ClientController } from "../controllers/admin/ClientController";
import { BidCampaignController } from "../controllers/admin/BidCampaignController";
import { getEnvironmentVariables } from "../environments/env";

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
      cb(null, getEnvironmentVariables().imageUploadPath)
  },
  filename: function (req, file, cb) {
      const ext = path.extname(file.originalname);
      cb(null, file.fieldname + '-' + Date.now() + ext);
  }
})
 
const upload = multer({ storage: storage })

class AdminRouter {
  public router: Router;
  constructor() {
    this.router = Router();
    this.getRoutes();
    this.postRoutes();
    this.putRoutes();
    this.deleteRoutes();
  }

  // Fetch routes
  getRoutes() { 
 
  }

  // Create / Auth routes
  postRoutes() {
    this.router.post( "/login", AdminController.login);
    this.router.post( "/employee", EmployeeController.create);
    this.router.post( "/client", ClientController.create );
    this.router.post( "/bid-campaign", upload.single("image"), BidCampaignController.create );
    this.router.post( "/bid-campaign/send-multiple", /*This is fake ;) (Just to use checkboxes as array), No image will upload here*/ upload.single("image"), BidCampaignController.sendToMultiple );
  }

  // Update routes
  putRoutes() {
    this.router.put( "/bid-campaign",  BidCampaignController.update );
  }
  
  // Delete
  deleteRoutes() {}
}

export default new AdminRouter().router;
