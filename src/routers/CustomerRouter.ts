import { Router } from "express";
import { CustomerController } from "../controllers/customer/CustomerController";
import { GlobalMiddleWares } from "../middlewares/GlobalMiddleWares";
import { CustomerValidators } from "../validators/CustomerValidators";

class CustomerRouter {
  public router: Router;
  constructor() {
    this.router = Router();
    this.getRoutes();
    this.postRoutes();
    this.deleteRoutes();
  }

  getRoutes() { 
 
  }

  postRoutes() {
    this.router.post( "/login", CustomerValidators.login(), CustomerController.login);
  }
  
  deleteRoutes() {}
}

export default new CustomerRouter().router;
