import { Router } from "express";
import * as multer from 'multer';
import * as path from 'path';

import { AdminController } from "../controllers/admin/AdminController";
import { EmployeeController } from "../controllers/admin/EmployeeController";
import { ClientController } from "../controllers/admin/ClientController";
import { BidCampaignController } from "../controllers/admin/BidCampaignController";
import { CapturedResponseController } from "../controllers/admin/general/CapturedResponseController";

class GeneralRouter {
  public router: Router;
  constructor() {
    this.router = Router();
    this.getRoutes();
    this.postRoutes();
    this.putRoutes();
    this.deleteRoutes();
  }

  // Fetch routes
  getRoutes() { 
 
  }

  // Create / Auth routes
  postRoutes() {
    this.router.post( "/capture-response", CapturedResponseController.create);
  }

  // Update routes
  putRoutes() {

  }
  
  // Delete
  deleteRoutes() {}
}

export default new GeneralRouter().router;
