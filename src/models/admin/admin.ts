import { DataTypes, Sequelize } from "sequelize";

export class Admin {
    static init(sequelize: Sequelize) {
        return sequelize.define(
            "admin",
            {
                name : {
                    type: DataTypes.STRING
                },
                email : {
                    type: DataTypes.STRING
                },
                password : {
                    type: DataTypes.TEXT
                },
                createdAt: {
                    type: DataTypes.BIGINT,
                    defaultValue: Date.now()
                },
                updatedAt: {
                    type: DataTypes.BIGINT,
                    defaultValue: Date.now()
                },
            },
            {
                freezeTableName: true
            }
        );
    }
}
