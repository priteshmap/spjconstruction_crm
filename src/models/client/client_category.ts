import { DataTypes, Sequelize } from "sequelize";

export class ClientCategory {
    static init(sequelize: Sequelize) {
        return sequelize.define(
            "client_category",
            {
                name : {
                    type: DataTypes.STRING
                },
                status : {
                    type: DataTypes.NUMBER
                },
                createdAt: {
                    type: DataTypes.BIGINT,
                    defaultValue: Date.now()
                },
                updatedAt: {
                    type: DataTypes.BIGINT,
                    defaultValue: Date.now()
                },
            },
            {
                freezeTableName: true
            }
        );
    }
}
