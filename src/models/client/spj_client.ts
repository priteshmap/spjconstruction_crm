import { DataTypes, Sequelize } from "sequelize";

export class SpjClient {
    static init(sequelize: Sequelize) {
        return sequelize.define(
            "spj_client",
            {
                name : {
                    type: DataTypes.STRING
                },
                email : {
                    type: DataTypes.STRING
                },
                phone : {
                    type: DataTypes.STRING
                },
                client_category_id : {
                    type: DataTypes.NUMBER
                },
                status : {
                    type: DataTypes.NUMBER
                },
                createdAt: {
                    type: DataTypes.BIGINT,
                    defaultValue: Date.now()
                },
                updatedAt: {
                    type: DataTypes.BIGINT,
                    defaultValue: Date.now()
                },
            },
            {
                freezeTableName: true
            }
        );
    }
}
