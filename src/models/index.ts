import { Sequelize } from "sequelize";
import { getEnvironmentVariables } from "../environments/env";
import { Admin } from "./admin/admin";
import { BidCampaign } from "./bid_campaign/bid_campaign";
import { CapturedResponse } from "./bid_campaign/captured_response";
import { ClientCategory } from "./client/client_category";
import { SpjClient } from "./client/spj_client";
import { Employee } from "./employee/employee";

let dbOptions = getEnvironmentVariables().db_options;
var sequelize = new Sequelize(dbOptions.db, dbOptions.username, dbOptions.password, {
    host: dbOptions.host,
    dialect: 'mysql'
});
sequelize.authenticate().then((success)=> console.log('connected')).catch((err) => console.log('problem in connecting to sequelize:', err));

const Model = {
    Admin: Admin.init(sequelize),
    Employee: Employee.init(sequelize),
    ClientCategory: ClientCategory.init(sequelize),
    SpjClient: SpjClient.init(sequelize),
    BidCampaign: BidCampaign.init(sequelize),
    CapturedResponse: CapturedResponse.init(sequelize),
}

export default Model;