import { DataTypes, Sequelize } from "sequelize";
 
export class CapturedResponse {
    static init(sequelize: Sequelize) {
        return sequelize.define(
            "captured_response",
            {
                bid_campaign_id : {
                    type: DataTypes.NUMBER
                },
                client_id : {
                    type: DataTypes.NUMBER
                },
                bid_amount : {
                    type: DataTypes.NUMBER
                },
                createdAt: {
                    type: DataTypes.BIGINT,
                    defaultValue: Date.now()
                },
                updatedAt: {
                    type: DataTypes.BIGINT,
                    defaultValue: Date.now()
                },
            },
            {
                freezeTableName: true
            }
        );
    }
}
