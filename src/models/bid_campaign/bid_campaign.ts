import { DataTypes, Sequelize } from "sequelize";
 
export class BidCampaign {
    static init(sequelize: Sequelize) {
        return sequelize.define(
            "bid_campaign",
            {
                name : {
                    type: DataTypes.STRING
                },
                description : {
                    type: DataTypes.TEXT
                },
                image : {
                    type: DataTypes.STRING
                },
                min_amount : {
                    type: DataTypes.NUMBER
                },
                max_amount : {
                    type: DataTypes.NUMBER
                },
                status : {
                    type: DataTypes.NUMBER
                },
                createdAt: {
                    type: DataTypes.BIGINT,
                    defaultValue: Date.now()
                },
                updatedAt: {
                    type: DataTypes.BIGINT,
                    defaultValue: Date.now()
                },
            },
            {
                freezeTableName: true
            }
        );
    }
}
