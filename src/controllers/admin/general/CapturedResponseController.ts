import Model from '../../../models';

export class CapturedResponseController {

  // POST
  static async create(req, res, next) {
    try{
        const { bid_campaign_id, email, bid_amount } = req.body;

        // Fetch client detail using email.
        const client:any = await Model.SpjClient.findOne({ where: { email: email } });
        if( !client ) {
            req.errorStatus = 404;
            throw new Error("Invalid client. Email does not exists in the system.");
        }

        // Fetch captured detail of client (if already bid is done)
        const capturedResponse: any = await Model.CapturedResponse.findOne({ where: { bid_campaign_id: bid_campaign_id, client_id: client.id } });

        if( capturedResponse ) {
            await Model.CapturedResponse.update({ bid_amount: bid_amount }, { where: { id: capturedResponse.id } });
            return res.status(200).json({ status: 200, msg: "Your bidding amount has been updated!" });
        } else {
          const data = {
            bid_campaign_id: bid_campaign_id,
            client_id: client.id,
            bid_amount: bid_amount,
          };
  
          const result = await Model.CapturedResponse.create(data);
  
          return res.status(200).json({ status: 200, msg: "Your bidding amount has been captured!", data: result });
        }
    } catch (err) {
        next(err);
    }
  }

  // GET
  static async read(req, res, next) {
    try{
        const result: any = await Model.CapturedResponse.findAll({
          order: [
            ['bid_amount', 'DESC']
          ]
        });
        for(let i=0; i<result.length; i++) {
          const client: any = await Model.SpjClient.findOne({ where: { id: result[i].client_id } });
          if(client)
            result[i].client = client;
          const bidCampaign: any = await Model.BidCampaign.findOne({ where: { id: result[i].bid_campaign_id } });
          if(bidCampaign)
            result[i].bidCampaign = bidCampaign;
        }

        req.capturedResponses = result;

        next();
    } catch (err) {
        next(err);
    }
  }

  // GET
  static async readById(req, res, next) {
    try{
        const { id } = req.params;

        let capturedResponse:any = await Model.CapturedResponse.findOne({ where: { id: id } });

        const client: any = await Model.SpjClient.findOne({ where: { id: capturedResponse.client_id } });
        if(client)
          capturedResponse.client = client;
        const campaign: any = await Model.BidCampaign.findOne({ where: { id: capturedResponse.bid_campaign_id } });
        if(campaign)
          capturedResponse.campaign = campaign;
        
        return res.status(200).json({ status: 200, msg: "Captured response fetched", data: capturedResponse });
    } catch (err) {
        next(err);
    }
  }

  
//   // POST
//   static async update(req, res, next) {
//     try{
//         const { id } = req.params;
//         const data = {
//             name: req.body.name,
//         };

//         await Model.SpjClient.update(data, { where: { id: id } });
//         const spjClient = await Model.SpjClient.findOne({ where: { id: id } });

//         return res.status(200).json({ status: 200, msg: "Client updated", data: spjClient });
//     } catch (err) {
//         next(err);
//     }
//   }

  // GET
  static async readBiddingsByCampaign(req, res, next) {
    try{
        const bid_campaign_id = req.params.id;

        const result: any = await Model.CapturedResponse.findAll({
          where: {
            bid_campaign_id: bid_campaign_id
          },
          order: [
            ['bid_amount', 'DESC']
          ]
        });
        for(let i=0; i<result.length; i++) {
          const client: any = await Model.SpjClient.findOne({ where: { id: result[i].client_id } });
          if(client)
            result[i].client = client;
        }

        req.biddingsByCampaign = result;

        next();
    } catch (err) {
        next(err);
    }
  }
}
