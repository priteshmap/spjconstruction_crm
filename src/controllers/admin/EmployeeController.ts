import * as Jwt from 'jsonwebtoken';
import { getEnvironmentVariables } from '../../environments/env';
import Model from '../../models';

export class EmployeeController {

  // POST
  static async create(req, res, next) {
    try{
        const data = {
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            email: req.body.email,
            mobile: req.body.mobile,
        };

        const result = await Model.Employee.create(data);

        return res.status(200).json({ status: 200, msg: "Employee created", data: result });
    } catch (err) {
        next(err);
    }
  }

  // GET
  static async read(req, res, next) {
    try{
        const result = await Model.Employee.findAll();

        req.employeeDetails = result;

        next();
    } catch (err) {
        next(err);
    }
  }

  // GET
  static async readById(req, res, next) {
    try{
        const { id } = req.params;

        let projectCategory:any = await Model.Employee.findOne({ where: { id: id } });
        
        return res.status(200).json({ status: 200, msg: "Employee fetched", data: projectCategory });
    } catch (err) {
        next(err);
    }
  }

  
  // POST
  static async update(req, res, next) {
    try{
        const { id } = req.params;
        const data = {
            name: req.body.name,
        };

        await Model.Employee.update(data, { where: { id: id } });
        const employeeDetail = await Model.Employee.findOne({ where: { id: id } });

        return res.status(200).json({ status: 200, msg: "Employee updated", data: employeeDetail });
    } catch (err) {
        next(err);
    }
  }

}
