import * as jwt from 'jsonwebtoken';
import { getEnvironmentVariables } from '../../environments/env';

import Model from '../../models';
import { Mailer } from '../../utils/Mailer';

export class BidCampaignController {

  // POST
  static async create(req, res, next) {
    try{
        const data:any = {
            name: req.body.name,
            description: req.body.description,
            min_amount: req.body.min_amount,
            max_amount: req.body.max_amount,
            status: 1,
        };

        // Check if image is uploaded (image is optional)
        if( req.file && req.file.filename ) {
          data.image = req.file.filename;
        }

        const result = await Model.BidCampaign.create(data);

        return res.status(200).json({ status: 200, msg: "Bid Campaign created", data: result });
    } catch (err) {
        next(err);
    }
  }

  // GET
  static async read(req, res, next) {
    try{
        const result = await Model.BidCampaign.findAll();

        req.bidCampaigns = result;

        next();
    } catch (err) {
        next(err);
    }
  }

  // GET
  static async readById(req, res, next) {
    try{
        let id = req.params.id || req.foundClient.bidCampaignId;

        let bidCampaign:any = await Model.BidCampaign.findOne({ where: { id: id } });
        
        req.bidCampaign = bidCampaign;
        
        next();
    } catch (err) {
        next(err);
    }
  }

  
  // POST
  static async update(req, res, next) {
    try{
        const { id } = req.params;
        const data: any = {
          name: req.body.name,
          description: req.body.description,
          min_amount: req.body.min_amount,
          max_amount: req.body.max_amount,
          status: 1,
        };

        // Check if image is uploaded (image is optional)
        if( req.file && req.file.filename ) {
          data.image = req.file.filename;
        }

        await Model.BidCampaign.update(data, { where: { id: id } });
        const bidCampaign = await Model.BidCampaign.findOne({ where: { id: id } });

        return res.status(200).json({ status: 200, msg: "Bid Campaign updated", data: bidCampaign });
    } catch (err) {
        next(err);
    }
  }

  // POST
  /** 
   * 1. Take all categories
   * 2. For each category
   *    2.1. Find clients.
   *    2.2. For each client.
   *       2.2.1. Send mail with campaign details.
  */
  static async sendToMultiple(req, res, next) {
    try{
      console.log(req.body);
        const { campaignId } = req.body;
        const categories = req.body.client_categories;

        let clientEmails: any = [];
        for(let client_category_id of categories) {
          const clients = await Model.SpjClient.findAll({ where: { client_category_id: client_category_id } });
          clients.forEach( (client:any) => {
            if(client.email)
              clientEmails.push(client.email);
          })
        }

        const campaignInfo:any = await Model.BidCampaign.findOne({ where: { id: campaignId } });

        for(let email of clientEmails ) {
          const token = await jwt.sign({
            email: email,
            bidCampaignId: campaignInfo.id
          }, getEnvironmentVariables().jwt_secret, { expiresIn: 60 * 60 }); // expires in 1 hr
          let url = getEnvironmentVariables().base_url+"/bid-campaign/bidding/"+token;
          const response = await Mailer.sendEmail({
            to: [email],
            subject: "Start Bidding",
            template: "email-template/sendbid.ejs",
            data: {
              campaignInfo: campaignInfo,
              url: url
            }
          });
        }

        return res.status(200).json({ status: 200, msg: "Bid Campaign email sent" });
    } catch (err) {
        next(err);
    }
  }
}
