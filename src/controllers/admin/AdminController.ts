import * as Jwt from 'jsonwebtoken';
import { getEnvironmentVariables } from '../../environments/env';
import Model from '../../models';

export class AdminController {

    // POST
    static async login(req, res, next) {
        try{
            const { email, password } = req.body;
            var datatable: any = await Model.Admin.findOne({ where:{ email: email, password: password } }); 
            if( datatable ){
                const payload = {
                    id: datatable.id,
                    email: datatable.email,
                };
                const env = getEnvironmentVariables();
                const token = await Jwt.sign(payload, env.jwt_secret, { expiresIn: env.jwt_expires_in });
                req.session.adminUser = datatable;
                return res.status(200).json({ status: 200, msg:'Wellcome To The SPJ Constructions', data: datatable , authToken: token});
            }else{
                const error = new Error("Please check email and password.");
                next(error);
            }
        } catch(err) {
            next(err);
        }
    }

}
