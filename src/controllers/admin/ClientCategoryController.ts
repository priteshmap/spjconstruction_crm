import Model from '../../models';

export class ClientCategoryController {

  // POST
  static async create(req, res, next) {
    try{
        const data = {
            name: req.body.name,
            status: 1,
        };

        const result = await Model.ClientCategory.create(data);

        return res.status(200).json({ status: 200, msg: "Client Category created", data: result });
    } catch (err) {
        next(err);
    }
  }

  // GET
  static async read(req, res, next) {
    try{
        const result = await Model.ClientCategory.findAll();

        req.clientCategories = result;

        next();
    } catch (err) {
        next(err);
    }
  }

  // GET
  static async readById(req, res, next) {
    try{
        const { id } = req.params;

        let projectCategory:any = await Model.ClientCategory.findOne({ where: { id: id } });
        
        return res.status(200).json({ status: 200, msg: "Client Category fetched", data: projectCategory });
    } catch (err) {
        next(err);
    }
  }

  
  // POST
  static async update(req, res, next) {
    try{
        const { id } = req.params;
        const data = {
            name: req.body.name,
        };

        await Model.ClientCategory.update(data, { where: { id: id } });
        const clientCategory = await Model.ClientCategory.findOne({ where: { id: id } });

        return res.status(200).json({ status: 200, msg: "Client Category updated", data: clientCategory });
    } catch (err) {
        next(err);
    }
  }

}
