import Model from '../../models';

export class ClientController {

  // POST
  static async create(req, res, next) {
    try{
        const data = {
            name: req.body.name,
            email: req.body.email,
            phone: req.body.phone,
            client_category_id: req.body.client_category_id,
            status: 1,
        };

        const result = await Model.SpjClient.create(data);

        return res.status(200).json({ status: 200, msg: "Client created", data: result });
    } catch (err) {
        next(err);
    }
  }

  // GET
  static async read(req, res, next) {
    try{
        const result: any = await Model.SpjClient.findAll();
        for(let i=0; i<result.length; i++) {
          const category: any = await Model.ClientCategory.findOne({ where: { id: result[i].id } });
          if(category)
            result[i].category = category.name;
        }

        req.spjClients = result;

        next();
    } catch (err) {
        next(err);
    }
  }

  // GET
  static async readById(req, res, next) {
    try{
        const { id } = req.params;

        let spjClient:any = await Model.SpjClient.findOne({ where: { id: id } });
        
        return res.status(200).json({ status: 200, msg: "Client fetched", data: spjClient });
    } catch (err) {
        next(err);
    }
  }

  
  // POST
  static async update(req, res, next) {
    try{
        const { id } = req.params;
        const data = {
            name: req.body.name,
        };

        await Model.SpjClient.update(data, { where: { id: id } });
        const spjClient = await Model.SpjClient.findOne({ where: { id: id } });

        return res.status(200).json({ status: 200, msg: "Client updated", data: spjClient });
    } catch (err) {
        next(err);
    }
  }

}
