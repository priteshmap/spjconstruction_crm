import * as express from 'express';
import * as Jwt from 'jsonwebtoken';
import { getEnvironmentVariables } from '../../environments/env';

export class CustomerController {

  static async login(req, res, next) {
    try {
      const { email, password } = req.body;

      const data = {
        email: email
      };

      const env = getEnvironmentVariables();
      const jwtToken = await Jwt.sign(data, env.jwt_secret, { expiresIn: env.jwt_expires_in });
      res.json({
        status: 200,
        message: 'Logged in successfully',
        data: data,
        token: jwtToken
      });
    }catch(err) {
      next(err);
    }
  }

}
