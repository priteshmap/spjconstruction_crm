import { Router } from "express";
import { GlobalMiddleWares } from "../middlewares/GlobalMiddleWares";

class AuthView {
  public router: Router;
  constructor() {
    this.router = Router();
    this.getRoutes();
  }

  getRoutes() { 
    this.router.get('/login', (req, res) => {
      res.render('authentication/flows/basic/sign-in');
    })
  }
} 

export default new AuthView().router;
 