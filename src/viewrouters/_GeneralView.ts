import { Router } from "express";
import { BidCampaignController } from "../controllers/admin/BidCampaignController";
import { ClientCategoryController } from "../controllers/admin/ClientCategoryController";
import { ClientController } from "../controllers/admin/ClientController";
import { EmployeeController } from "../controllers/admin/EmployeeController";
import { GlobalMiddleWares } from "../middlewares/GlobalMiddleWares";

class GeneralView {
  public router: Router;
  constructor() {
    this.router = Router();
    this.getRoutes();
  }

  getRoutes() { 
    // Bidding (User opens from mail)
    this.router.get('/bid-campaign/bidding/:token', GlobalMiddleWares.identifyClient, BidCampaignController.readById, (req:any, res) => {
      const client = req.foundClient;
      const bidCampaign = req.bidCampaign;
      res.render('bidCampaign/captureBid', { bidCampaign: bidCampaign, client: client });
    })
  }
}

export default new GeneralView().router;
 