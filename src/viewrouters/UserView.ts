import { Router } from "express";
import { GlobalMiddleWares } from "../middlewares/GlobalMiddleWares";

class UserView {
  public router: Router;
  constructor() {
    this.router = Router();
    this.getRoutes();
  }

  getRoutes() { 
    this.router.get('/home', (req, res) => {
      res.render('index');
    }) 
  }
}

export default new UserView().router;
 