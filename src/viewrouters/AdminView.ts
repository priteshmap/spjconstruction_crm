import { Router } from "express";
import { BidCampaignController } from "../controllers/admin/BidCampaignController";
import { ClientCategoryController } from "../controllers/admin/ClientCategoryController";
import { ClientController } from "../controllers/admin/ClientController";
import { EmployeeController } from "../controllers/admin/EmployeeController";
import { CapturedResponseController } from "../controllers/admin/general/CapturedResponseController";
import { GlobalMiddleWares } from "../middlewares/GlobalMiddleWares";

class AdminView {
  public router: Router;
  constructor() {
    this.router = Router();
    this.getRoutes();
  }

  getRoutes() { 
    this.router.get('/home', GlobalMiddleWares.checkAdminSession, (req, res) => {
      res.render('index');
    }) 
    
    // Create Employee
    this.router.get('/employee', GlobalMiddleWares.checkAdminSession, (req, res) => {
      res.render('employee/create');
    }) 
    
    // Listing Employees
    this.router.get('/employee/all', GlobalMiddleWares.checkAdminSession, EmployeeController.read, (req:any, res) => {
      const employeeDetails = req.employeeDetails;
      res.render('employee/index', { employeeDetails: employeeDetails });
    }) 
    
    // Create Client
    this.router.get('/client', GlobalMiddleWares.checkAdminSession, ClientCategoryController.read, (req:any, res) => {
      const clientCategories = req.clientCategories;
      res.render('client/create', { clientCategories: clientCategories });
    }) 
    
    // Listing Clients
    this.router.get('/client/all', GlobalMiddleWares.checkAdminSession, ClientController.read, (req:any, res) => {
      const clients = req.spjClients;
      res.render('client/index', { clients: clients });
    }) 
    
    // Create Bid Campaign
    this.router.get('/bid-campaign', GlobalMiddleWares.checkAdminSession, (req:any, res) => {
      res.render('bidCampaign/create', {  });
    }) 
    
    // Listing Bid Campaign
    this.router.get('/bid-campaign/all', GlobalMiddleWares.checkAdminSession, BidCampaignController.read, (req:any, res) => {
      const bidCampaigns = req.bidCampaigns;
      res.render('bidCampaign/index', { bidCampaigns: bidCampaigns });
    }) 
    
    // Send Bid Campaign to Multiple Category
    this.router.get('/bid-campaign/send', GlobalMiddleWares.checkAdminSession, BidCampaignController.read, ClientCategoryController.read, (req:any, res) => {
      const bidCampaigns = req.bidCampaigns;
      const clientCategories = req.clientCategories;
      res.render('bidCampaign/send', { bidCampaigns: bidCampaigns, clientCategories: clientCategories });
    }) 
    
    // Send Bid Campaign to Multiple Category
    this.router.get('/bid-campaign/live', GlobalMiddleWares.checkAdminSession, CapturedResponseController.read, (req:any, res) => {
      const capturedResponses = req.capturedResponses;
      res.render('bidCampaign/live', { capturedResponses: capturedResponses });
    }) 
    
    // Show overview of single campaign. Live status
    this.router.get('/bid-campaign/:id', GlobalMiddleWares.checkAdminSession, BidCampaignController.readById, CapturedResponseController.readBiddingsByCampaign, (req:any, res) => {
      const biddingsByCampaign = req.biddingsByCampaign;
      const bidCampaign = req.bidCampaign;
      bidCampaign.image = bidCampaign.image ? "uploads/"+bidCampaign.image : "assets/media/no_image.jpg";
      res.render('bidCampaign/overview', { biddingsByCampaign: biddingsByCampaign, bidCampaign:bidCampaign });
    }) 
  }
}

export default new AdminView().router;
 