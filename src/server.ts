import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as path from 'path';
import * as session from 'express-session';
import * as ejs from 'ejs';

import { getEnvironmentVariables } from './environments/env';
import CustomerRouter from './routers/CustomerRouter';
import UserView from './viewrouters/UserView';
import AuthView from './viewrouters/AuthView';
import AdminRouter from './routers/AdminRouter';
import AdminView from './viewrouters/AdminView';
import { Mailer } from './utils/Mailer';
import _GeneralView from './viewrouters/_GeneralView';
import _GeneralRouter from './routers/_GeneralRouter';

export class Server {
    // public app : express.Application = express();
    public app : any = express();

    constructor() {
        this.setConfigurations();
        this.setRoutes();
        this.setViewRoutes();
        this.error404Handler();
        this.handleErrors();
    }
    
    setConfigurations() {
        this.configureBodyparser();
        this.configureEjs();
        this.configureSession();
    }

    configureBodyparser() {
        this.app.use(bodyParser.urlencoded({ extended: false }));
        this.app.use(express.json());
    }

    configureEjs() {
        this.app.use(express.static( path.join(__dirname , 'public') ));
        this.app.set('views', path.join(__dirname, 'views'));
        this.app.set('view engine', 'ejs');
    }

    configureSession() {
        const ss = {
            secret: 'my top secret',
            cookie:  { },
            resave: false,
            saveUninitialized: true
        };
        if( process.env.NODE_ENV == "production" ) {
            this.app.set('trust proxy', 1) // trust first proxy
            //ss.cookie.secure = true // serve secure cookies
        }
        this.app.use(session(ss));
    }   

    setRoutes() {
        this.app.use('/api/admin', AdminRouter);
        this.app.use('/api/customer', CustomerRouter);
        this.app.use('/api/general', _GeneralRouter);

        // ! testing mail
        // this.app.get('/testingmailer', async (req, res, next) => {
        //     try {
        //         const response = await Mailer.sendEmail({
        //             to: ["shubhamthesmat@gmail.com"],
        //             subject: "Testing mail",
        //             template: "email-template/sendbid",
        //             data: {
        //                 name: "Shubham"
        //             }
        //         });
        //         res.json("Sent");
        //     }catch(err) {
        //         next(err);
        //     }
        // })
    }

    setViewRoutes() {
        // Set login as first page
        this.app.get('/', (req, res) => {
            res.redirect('/auth/login');
        })
        this.app.use(_GeneralView);
        this.app.use('/auth', AuthView);
        this.app.use('/admin', AdminView);
        this.app.use('/user', UserView);
    }

    error404Handler() {
        this.app.use((req, res) => {
          res.status(404).json({
              message: 'Not found',
              status: 404
          })  
        })
    }

    handleErrors() {
        this.app.use((error, req, res, next) => {
            const errorStatus = req.errorStatus || 500;
            if( errorStatus == 500 && process.env.NODE_ENV == "production" )
                error.message = "Something went wrong. Please try again.";
            res.status(errorStatus).json({
                message: error.message || 'Something went wrong. Please try again.',
                status: errorStatus
            });
        })
    }
}