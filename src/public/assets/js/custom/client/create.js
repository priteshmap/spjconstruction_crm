"use strict";

// Class definition
var CreateClient = function() {
    // Elements
    var form;
    var submitButton;
    var validator;

    // Handle form
    var handleForm = function(e) {
        // Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
        validator = FormValidation.formValidation(
			form,
			{
				fields: {					
					'name': {
                        validators: {
							notEmpty: {
								message: 'Name is required'
							}
						}
					},
					'email': {
                        validators: {
							notEmpty: {
								message: 'Email address is required'
							},
                            emailAddress: {
								message: 'The value is not a valid email address'
							}
						}
					},
					'phone': {
                        validators: {
							notEmpty: {
								message: 'Mobile number is required'
							},
						}
					},
                    'client_category_id': {
                        validators: {
                            notEmpty: {
                                message: 'Client Category is required'
                            },
                            // callback: {
                            //     message: 'Please enter valid mobile number',
                            //     callback: function(input) {
                            //         if (input.value.length < 10) {
                            //             return false;
                            //         }
                            //     }
                            // } 
                        }
                    } 
				},
				plugins: {
					trigger: new FormValidation.plugins.Trigger(),
					bootstrap: new FormValidation.plugins.Bootstrap5({
                        rowSelector: '.fv-row',
                        eleInvalidClass: '',
                        eleValidClass: ''
                    })
				}
			}
		);		

        // Handle form submit
        submitButton.addEventListener('click', function (e) {
            // Prevent button default action
            e.preventDefault();

            // Validate form
            validator.validate().then(function (status) {
                if (status == 'Valid') {
                    // Show loading indication
                    submitButton.setAttribute('data-kt-indicator', 'on');

                    // Disable button to avoid multiple click 
                    submitButton.disabled = true;

                    const data = {
                        name: form.querySelector('[name="name"]').value,
                        email: form.querySelector('[name="email"]').value,
                        phone: form.querySelector('[name="phone"]').value,
                        client_category_id: form.querySelector('[name="client_category_id"]').value
                    };

                    // Simulate ajax request
                    fetch('/api/admin/client', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify(data)
                    }).then(data => data.json())                    
                    .then(function(res){
                        console.log(res);
                        if(res.status == 200) {
                            Swal.fire({
                                text: "Client is successfully created!",
                                icon: "success",
                                buttonsStyling: false,
                                confirmButtonText: "Ok, got it!",
                                customClass: {
                                    confirmButton: "btn btn-primary"
                                }
                            }).then(function (result) {
                                if (result.isConfirmed) { 
                                    form.querySelector('[name="name"]').value=""
                                    form.querySelector('[name="email"]').value=""
                                    form.querySelector('[name="phone"]').value=""
                                    location.href="/admin/client/all"
                                }
                            });
                        }else {
                            Swal.fire({
                                text: res.message,
                                icon: "error",
                                buttonsStyling: false,
                                confirmButtonText: "Ok, got it!",
                                customClass: {
                                    confirmButton: "btn btn-primary"
                                }
                            });
                            // Hide loading indicator
                            submitButton.setAttribute('data-kt-indicator', 'off');
                            // Enable submit button
                            submitButton.disabled = false;
                        }
                    })
                } else {
                    // Show error popup. For more info check the plugin's official documentation: https://sweetalert2.github.io/
                    Swal.fire({
                        text: "Sorry, looks like there are some errors detected, please try again.",
                        icon: "error",
                        buttonsStyling: false,
                        confirmButtonText: "Ok, got it!",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    });
                }
            });
		});
    }

    // Public functions
    return {
        // Initialization
        init: function() {
            form = document.querySelector('#create_client_form');
            submitButton = document.querySelector('#create_client_submit');
            
            handleForm();
        }
    };
}();

// On document ready
KTUtil.onDOMContentLoaded(function() {
    CreateClient.init();
});
