"use strict";

// Handle form submit
$("#create_campaign_form").submit(function (e) {
    // Prevent button default action
    e.preventDefault();
    console.log(this);

            const formData = new FormData(this);

            // Simulate ajax request
            fetch('/api/admin/bid-campaign', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: formData
            }).then(data => data.json())                    
            .then(function(res){
                console.log(res);
                if(res.status == 200) {
                    Swal.fire({
                        text: "Campaign is successfully created!",
                        icon: "success",
                        buttonsStyling: false,
                        confirmButtonText: "Ok, got it!",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    }).then(function (result) {
                        if (result.isConfirmed) { 
                            document.querySelector('[name="name"]').value=""
                            document.querySelector('[name="description"]').value=""
                            document.querySelector('[name="min_amount"]').value=""
                            document.querySelector('[name="max_amount"]').value=""
                            location.href="/admin/bid-campaign/all"
                        }
                    });
                }else {
                    Swal.fire({
                        text: res.message,
                        icon: "error",
                        buttonsStyling: false,
                        confirmButtonText: "Ok, got it!",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    });
                }
            })
});

// // Class definition
// var CreateCampaign = function() {
//     // Elements
//     var form;
//     var submitButton;
//     var validator;

//     // Handle form
//     var handleForm = function(e) {
//         // Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
//         validator = FormValidation.formValidation(
// 			form,
// 			{
// 				fields: {					
// 					'name': {
//                         validators: {
// 							notEmpty: {
// 								message: 'Campaign Name is required'
// 							}
// 						}
// 					},
// 					'description': {
//                         validators: {
// 							notEmpty: {
// 								message: 'Description is required'
// 							}
// 						}
// 					},
// 					'min_amount': {
//                         validators: {
// 							notEmpty: {
// 								message: 'Min bid amount is required'
// 							},
// 						}
// 					},
//                     'max_amount': {
//                         validators: {
//                             notEmpty: {
//                                 message: 'Max bid amount is required'
//                             }
//                         }
//                     } 
// 				},
// 				plugins: {
// 					trigger: new FormValidation.plugins.Trigger(),
// 					bootstrap: new FormValidation.plugins.Bootstrap5({
//                         rowSelector: '.fv-row',
//                         eleInvalidClass: '',
//                         eleValidClass: ''
//                     })
// 				}
// 			}
// 		);		

        
//     }

//     // Public functions
//     return {
//         // Initialization
//         init: function() {
//             //form = document.querySelector('#create_campaign_form');
//             submitButton = document.querySelector('#create_campaign_submit');
            
//             handleForm();
//         }
//     };
// }();

// // On document ready
// KTUtil.onDOMContentLoaded(function() {
//     CreateCampaign.init();
// });
