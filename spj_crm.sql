-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 12, 2021 at 10:14 AM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `spj_crm`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `password` text NOT NULL,
  `createdAt` bigint(20) NOT NULL,
  `updatedAt` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `password`, `createdAt`, `updatedAt`) VALUES
(1, 'Shubham', 'shubham@gmail.com', 'shubham', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `bid_campaign`
--

CREATE TABLE `bid_campaign` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(250) NOT NULL,
  `min_amount` int(11) NOT NULL,
  `max_amount` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `createdAt` bigint(20) NOT NULL,
  `updatedAt` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bid_campaign`
--

INSERT INTO `bid_campaign` (`id`, `name`, `description`, `image`, `min_amount`, `max_amount`, `status`, `createdAt`, `updatedAt`) VALUES
(10, 'Demo name', 'Demo description', 'image-1623431266097.jpg', 10, 20, 1, 1623431178849, 1623431178849),
(11, 'Demo name', 'Demo description', 'image-1623431266097.jpg', 1000, 2000, 1, 1623431178849, 1623431178849),
(16, 'Laptop', 'With Accessories', 'image-1623485344563.jpg', 50000, 100000, 1, 1623484846632, 1623484846632);

-- --------------------------------------------------------

--
-- Table structure for table `captured_response`
--

CREATE TABLE `captured_response` (
  `id` int(11) NOT NULL,
  `bid_campaign_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `bid_amount` decimal(20,2) NOT NULL,
  `createdAt` bigint(20) NOT NULL,
  `updatedAt` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `captured_response`
--

INSERT INTO `captured_response` (`id`, `bid_campaign_id`, `client_id`, `bid_amount`, `createdAt`, `updatedAt`) VALUES
(1, 10, 2, '15.00', 1623448007274, 1623484846633),
(2, 10, 1, '20.00', 1623448007274, 1623448007274),
(3, 16, 2, '53000.00', 1623484846633, 1623484846633);

-- --------------------------------------------------------

--
-- Table structure for table `client_category`
--

CREATE TABLE `client_category` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `status` int(11) NOT NULL,
  `createdAt` bigint(20) NOT NULL,
  `updatedAt` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client_category`
--

INSERT INTO `client_category` (`id`, `name`, `status`, `createdAt`, `updatedAt`) VALUES
(1, 'Technology', 1, 246546466, 246546466),
(2, 'Entertainment', 1, 2465464667, 2465464667),
(3, 'Knowledge', 1, 2465464667, 2465464667),
(4, 'IT', 1, 2465464667, 2465464667);

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `id` int(11) NOT NULL,
  `first_name` varchar(250) NOT NULL,
  `last_name` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `mobile` varchar(250) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `createdAt` bigint(20) NOT NULL,
  `updatedAt` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`id`, `first_name`, `last_name`, `email`, `mobile`, `status`, `createdAt`, `updatedAt`) VALUES
(1, 'Shubham', 'Singh', 'shubham@gmail.com', '9111883489', 0, 1623244788937, 1623244788937),
(2, 'Pritesh', 'Singh', 'pritesh@gmail.com', '7485485658', 0, 1623244788937, 1623244788937),
(3, 'Aryan', 'Shukla', 'aryan@gmail.com', '8574565895', 0, 1623334003723, 1623334003723),
(4, 'Harsh', 'Patel', 'harsh@gmail.com', '9854585222', 0, 1623334003723, 1623334003723),
(5, 'Harshdeep', 'Singh', 'harshd@gmail.com', '9653254658', 0, 1623334003723, 1623334003723),
(6, 'Shubham', 'singh', 'shubhamthesmat@gmail.com', '7485444474', 0, 1623429955605, 1623429955605);

-- --------------------------------------------------------

--
-- Table structure for table `spj_client`
--

CREATE TABLE `spj_client` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `phone` varchar(250) NOT NULL,
  `client_category_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `createdAt` bigint(20) NOT NULL,
  `updatedAt` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spj_client`
--

INSERT INTO `spj_client` (`id`, `name`, `email`, `phone`, `client_category_id`, `status`, `createdAt`, `updatedAt`) VALUES
(1, 'Pritesh', 'shubh.singh.it@gmail.com', '8565984758', 1, 1, 654897879, 654897879),
(2, 'Shubham Singh', 'shubhamthesmat@gmail.com', '7485655554', 2, 1, 1623337471517, 1623337471517),
(3, 'Ritesh Mishra', 'incorrectemailisit@gmail.com', '7485652354', 3, 1, 1623342114532, 1623342114532);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `emailunique` (`email`);

--
-- Indexes for table `bid_campaign`
--
ALTER TABLE `bid_campaign`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `captured_response`
--
ALTER TABLE `captured_response`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client_category`
--
ALTER TABLE `client_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `mobile` (`mobile`);

--
-- Indexes for table `spj_client`
--
ALTER TABLE `spj_client`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `bid_campaign`
--
ALTER TABLE `bid_campaign`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `captured_response`
--
ALTER TABLE `captured_response`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `client_category`
--
ALTER TABLE `client_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `spj_client`
--
ALTER TABLE `spj_client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
